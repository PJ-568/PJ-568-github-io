### 个人信息

- **姓名：刘甜**
  - 男，零零后，水瓶座，在读本科生。

<img src="https://github-readme-stats.vercel.app/api/?username=PJ-568&include_all_commits=true&show_icons=true&title_color=fff&icon_color=79ff97&text_color=9f9f9f&bg_color=151515"/>
<img src="https://github-readme-stats.vercel.app/api/top-langs/?username=PJ-568&layout=donut-vertical&title_color=fff&icon_color=79ff97&text_color=9f9f9f&bg_color=151515"/>

### 网站活动

![Alt](https://repobeats.axiom.co/api/embed/142fde91ca8389d0ebc04a6947d741cffc1d6799.svg "Repobeats analytics image")

### 联系方式

- 微信：PJ568-
- QQ：[3463625335](tencent://message/?uin=3463625335&Menu=yes)
- Gitee：[@PJ-568](Https://Gitee.com/PJ-568/)
- Github：[@PJ-568](Https://Github.com/PJ-568/)
- 电子邮箱：[LiuTian-PJ568@PJ568.eu.org](mailto:LiuTian-PJ568@PJ568.eu.org)

### 关于网站

- 应用程序

  - 程序：[下载](../app/)

- 域名：[EU.ORG](https://nic.eu.org/arf/)

  - 优点：免费、顶级域名
  - 缺点：国内无法备案

- 页面主题：[Default](https://github.com/niemingzhao/niemingzhao.github.io/tree/theme)

  - 参考网站：[聂明照的博客](https://www.niemingzhao.top/)

- 域名解析：[Cloudflare](https://www.cloudflare.net)

  - 优点：免费、稳定、国际化
  - 缺点：国内访问迟缓

- 网页托管：[Github Pages](https://pages.github.com/)

  - 优点：免费、稳定、方便管理
  - 缺点：无后端、部分地区无法访问或访问缓慢

- 网站框架：[Hexo](https://hexo.io/)

  - 优点：静态网站、高性能、一键部署、高度可定制化
  - 缺点：首次部署配置需要时间、大规模构建消耗大量时间

<img width="600" src="https://api.star-history.com/svg?repos=PJ-568/PJ-568.github.io&type=Date" />